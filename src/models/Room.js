/**
 * @Author: Hưng <hungls>
 * @Email:  hunglsxx@gmail.com
 * @Last modified by:   Hưng
 */

var mongoose = require('mongoose');

const Schema = mongoose.Schema;
const STATUS_CREATED = 'created',
    STATUS_STARTED = 'started',
    STATUS_FINISHED = 'finished',
    STATUS_DISPOSED = 'disposed',
    STATUS_WAITING = 'waiting';

const room = new Schema({
    name: { type: String },
    peers: [{ type: Schema.Types.Mixed }],
    status: { type: String },
    max_client: { type: Number },
    password: { type: String },

    /**For game */
    row: { type: Number, required: true },
    column: { type: Number, required: true },
    matrix: [{ type: Number }],
    opened: [{ type: Number }],
    left: [{ type: Number }],
    players: [{ type: Schema.Types.Mixed }],
    curent: { type: Number },
    next: { type: Number },
    opened_color: { type: Schema.Types.Mixed },
    result: [{ type: Schema.Types.Mixed }],
    level: { type: String }

}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

room.statics.getStatus = function (status) {
    switch (status) {
        case "CREATED":
            return STATUS_CREATED;
        case "STARTED":
            return STATUS_STARTED;
        case "FINISHED":
            return STATUS_FINISHED;
        case "DISPOSED":
            return STATUS_DISPOSED;
        case "WAITING":
            return STATUS_WAITING;
        default:
            return "";
    }
}

room.methods.loadState = function (state) {
    state.matrix.forEach(element => {
        this.matrix.push(element);
    })
    this.status = state.status;
    state.players.forEach((value, at) => {
        var play = value.toJSON();
        at = String(at);
        play['session_id'] = at;
        this.players.push(play);
    })
    state.opened.forEach(val => {
        this.opened.push(val)
    })
}
module.exports = mongoose.model('room', room);
