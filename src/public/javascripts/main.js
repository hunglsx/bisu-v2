var host = (window.document.location.host || "localhost").replace(/:.*/, '');
var port = (window.document.location.port) ? ':' + window.document.location.port : '';
var colyseusClient = new Colyseus.Client('wss://' + host + port);
var mediasoupClient = window.mediasoupClient;

/** */

var isNS = (navigator.appName == "Netscape") ? 1 : 0;
if (navigator.appName == "Netscape") document.captureEvents(Event.MOUSEDOWN || Event.MOUSEUP);
function mischandler() {
    return false;
}
function mousehandler(e) {
    var myevent = (isNS) ? e : event;
    var eventbutton = (isNS) ? myevent.which : myevent.button;
    if ((eventbutton == 2) || (eventbutton == 3)) return false;
}
document.oncontextmenu = mischandler;
document.onmousedown = mousehandler;
document.onmouseup = mousehandler;
var isCtrl = false;
document.onkeyup = function (e) {
    if (e.which == 17)
        isCtrl = false;
}

document.onkeydown = function (e) {
    if (e.which == 123) return false;
    if (e.which == 17)
        isCtrl = true;
    if (((e.which == 85) || (e.which == 117) || (e.which == 65)
        || (e.which == 97) || (e.which == 67) || (e.which == 99)
        || (e.which == 70))
        && isCtrl == true) {
        // alert(‘Keyboard shortcuts are cool!’);
        return false;
    }
}
/** */


async function loadDevices() {
    try {
        await navigator.mediaDevices.getUserMedia({ audio: true, video: true });
        let devices = await navigator.mediaDevices.enumerateDevices();
        devices.forEach(device => {
            let el = null;
            if ('audioinput' === device.kind) {
                el = audioSelect;
            } else if ('videoinput' === device.kind) {
                el = videoSelect;
            }
            if (!el) return;

            let option = document.createElement('option');
            option.value = device.deviceId;
            option.innerText = device.label;
            el.appendChild(option);
        })
    } catch (error) {
        console.error("Fnc loadDevices", error.code, error.message);
        alert("Nếu bạn đang mở link từ ứng dụng facebook, hoặc zalo, bạn sẽ không thể  thực hiện các cuộc video call trong quá trình chơi. Vui lòng sử dụng trình duyệt khác để có trải nghiệm tốt hơn. Tôi không thích facebook!");
    }
}

var room = new RoomClient(colyseusClient, mediasoupClient);