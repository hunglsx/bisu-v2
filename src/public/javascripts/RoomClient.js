class RoomClient {
    constructor(colyseus, mediasoup) {
        this.mediaType = {
            audio: 'audioType',
            video: 'videoType',
            screen: 'screenType'
        };
        this.colyseus = colyseus;
        this.mediasoup = mediasoup;
        this.room = null;
        this.device = null;
        this.producerTransport = null;
        this.consumerTransport = null;
        this.consumers = new Map();
        this.producers = new Map();
        this.producerLabel = new Map();
    }

    addListeners() {
        try {
            if (!this.room) return;
            console.log('joined!');
            this.room.onMessage("*", (type, message) => {
                console.log("Received message:", type, "=>", message);
            });
            this.room.onLeave(function () {
                console.log("LEFT ROOM", arguments);
            });
            this.room.onStateChange(function (state) {
                console.log("State", state);
                this.renderMatrix(state.toJSON());
                this.renderSummary(state.toJSON());
            }.bind(this));

            this.room.onMessage("onGetRouterRtpCapabilities", async function (data) {
                console.log("onGetRouterRtpCapabilities", data);
                await this.loadDevice(data);
                this.initTransports();

                /** chỗ này cần sửa để tối ưu thành thư viện, sẽ sửa sau*/
                var i = 0;
                var loading = new bootstrap.Modal(document.getElementById('loadingModal'));
                loading.show();
                while (true) {
                    i++;
                    console.log("Waiting for fully transport...");
                    if (this.producerTransport != null && this.consumerTransport != null) {
                        console.log("Transport is fully");
                        this.room.send("getProducers");
                        break;
                    }
                    if (i == 100) break;
                    await this.sleep(1000);
                }
                loading.hide();

            }.bind(this));

            this.room.onMessage("onCreateProducerTransport", async function (data) {
                console.log("onCreateProducerTransport", data);
                this.producerTransport = this.device.createSendTransport(data);

                this.producerTransport.on('connect', async function ({ dtlsParameters }, callback, errback) {
                    try {
                        this.room.send("connectTransport", {
                            dtlsParameters,
                            transportId: data.id
                        });
                        this.room.onMessage("onConnectTransport", async (msg) => {
                            callback({ id: data.id });
                            console.log("onConnectTransport", msg);
                        });
                    } catch (error) {
                        console.log(error.message);
                        errback(error);
                    }
                }.bind(this));

                this.producerTransport.on('produce', async function ({
                    kind,
                    rtpParameters
                }, callback, errback) {
                    try {
                        this.room.send("produce", {
                            producerTransportId: this.producerTransport.id,
                            kind,
                            rtpParameters,
                        });
                        this.room.onMessage("onProduce", async function (producerId) {
                            console.log("onProduce", producerId);
                            callback({
                                id: producerId
                            });
                        })
                    } catch (err) {
                        console.log(err.message);
                        errback(err);
                    }
                }.bind(this));

                this.producerTransport.on('connectionstatechange', function (state) {
                    switch (state) {
                        case "connecting":
                            console.log("producerConnecting...");
                            break;
                        case "connected":
                            console.log("producerConnected!");
                            //localVideo.srcObject = stream;
                            break;
                        case "failed":
                            console.log("producerConnection Faled!");
                            this.producerTransport.close();
                            break;
                        default:
                            break;
                    }
                }.bind(this));
            }.bind(this));

            this.room.onMessage("onCreateConsumerTransport", async function (data) {
                console.log("onCreateConsumerTransport", data);
                // only one needed
                this.consumerTransport = this.device.createRecvTransport(data);

                this.consumerTransport.on('connect', function ({
                    dtlsParameters
                }, callback, errback) {
                    try {
                        this.room.send("connectTransport", {
                            dtlsParameters,
                            transportId: this.consumerTransport.id
                        });
                        this.room.onMessage("onConnectTransport", async (msg) => {
                            callback();
                            console.log("onConnectTransport", msg);
                        })
                    } catch (error) {
                        errback(error);
                    }
                }.bind(this));

                this.consumerTransport.on('connectionstatechange', async function (state) {
                    switch (state) {
                        case 'connecting':
                            break;

                        case 'connected':
                            //remoteVideo.srcObject = await stream;
                            //await socket.request('resume');
                            break;

                        case 'failed':
                            this.consumerTransport.close();
                            break;

                        default:
                            break;
                    }
                }.bind(this));

            }.bind(this));

            this.room.onMessage("newProducers", async function (data) {
                console.log('new producers', data);
                for (let { producerId } of data) {
                    const { rtpCapabilities } = this.device;
                    var consumerId = (this.consumerTransport != null) ? this.consumerTransport.id : '';
                    this.room.send("consume", {
                        rtpCapabilities,
                        consumerTransportId: consumerId,
                        producerId
                    });
                }
            }.bind(this));

            this.room.onMessage("consumerClosed", function (data) {
                const {
                    consumerId
                } = data;
                console.log('closing consumer:', consumerId);
                this.removeConsumer(consumerId);
            }.bind(this));

            this.room.onMessage("onConsume", async function (data) {
                try {
                    console.log("onConsume", data);
                    const { id, kind, rtpParameters, producerId } = data;
                    let codecOptions = {};
                    const consumer = await this.consumerTransport.consume({ id, producerId, kind, rtpParameters, codecOptions, })
                    const stream = new MediaStream();
                    stream.addTrack(consumer.track);
                    var res = { consumer, stream, kind }
                    await this.consume(res);
                } catch (error) {
                    console.log(error.message);
                }
            }.bind(this));

            this.room.onMessage("ffmpeg", async function (command) {
                console.log("ffmpeg", command);
                if ($("#ffmpeg").length) {
                    $("#ffmpeg").val(command);
                }
            }.bind(this));

            /**For game */

            this.room.onMessage("finish", (message) => {
                var prizeList = '';
                for (var i in message) {
                    var label = "";
                    if (i == 0) label = 'Chiến thắng';
                    prizeList += '<li class="list-group-item d-flex justify-content-between align-items-start">'
                        + '<div class="ms-2 me-auto" >'
                        + '<div class="fw-bold">' + message[i].name + '</div>'
                        + label
                        + '</div>'
                        + '<span class="badge rounded-pill" style="background-color:#' + message[i].color + '">' + message[i].open + '</span>'
                        + '</li>';
                }
                prizeList = '<ol class="list-group list-group-numbered">' + prizeList + '</ol>';
                $("#resultModal .modal-body").html(prizeList);
                var resultModal = new bootstrap.Modal(document.getElementById('resultModal'), {
                    keyboard: false
                });
                resultModal.show();
            });

            this.room.onMessage("receive_message", function (text) {
                var $messages, message;
                if (text.message.trim() === '') {
                    return;
                }
                $messages = $('.messages');
                message = new this.Message({
                    text: text.message,
                    message_side: 'left',
                    color: text.color
                });
                message.draw();
                return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
            }.bind(this));

        } catch (error) {
            console.error("Fnc create colyseus", error.code, error.message);
            alert(error.message);
        }
    }

    create(id) {
        this.colyseus.create('livestream', { code: "one", id: id }).then(function (room) {
            if (room) {
                this.room = room;
                this.addListeners();
            }
        }.bind(this)).catch(error => {
            console.error("Fnc create colyseus", error.code, error.message);
            alert(error.message);
        });
    }

    join(id) {
        this.colyseus.joinById(id, { code: "one" }).then(function (room) {
            this.room = room;
            this.addListeners();
        }.bind(this)).catch(error => {
            console.error("Fnc join colyseus", error.code, error.message);
            alert(error.message);
        });
    }

    joinOrCreate() {
        this.colyseus.joinOrCreate('livestream', { code: "one" }).then(function (room) {
            this.room = room;
            this.addListeners();
        }.bind(this)).catch(error => {
            console.error("Fnc joinOrCreate colyseus", error.code, error.message);
            alert(error.message);
        });
    }

    joinByLastId() {
        this.colyseus.joinById(room.id).then(function (room) {
            this.room = room;
            this.addListeners();
        }.bind(this)).catch(error => {
            console.error("Fnc joinByLastId colyseus", error.code, error.message);
            alert(error.message);
        });
    }

    getAvailableRooms() {
        this.colyseus.getAvailableRooms().then((rooms) => {
            console.log(rooms);
        }).catch(e => {
            console.error(e);
        });
    }

    reconnect() {
        this.colyseus.reconnect(this.room.id, this.room.sessionId).then(function (room) {
            this.room = room;
            this.addListeners();
        }.bind(this));
    }

    closeConnection() {
        this.room.connection.close();
    }

    async sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async loadDevice(routerRtpCapabilities) {
        try {
            this.device = new this.mediasoup.Device();
            await this.device.load({
                routerRtpCapabilities
            });
            return true;
        } catch (error) {
            if (error.name === 'UnsupportedError') {
                console.error('Browser not supported');
            }
            console.error("loadDevice", error.message);
            return false;
        }
    }

    initTransports() {
        this.room.send("createWebRtcTransport", {
            type: "onCreateProducerTransport",
            options: {
                forceTcp: false,
                rtpCapabilities: this.device.rtpCapabilities,
            }
        });

        this.room.send("createWebRtcTransport", {
            type: "onCreateConsumerTransport",
            options: {
                forceTcp: false,
            }
        });
    }

    removeConsumer(consumerId) {
        let elem = document.getElementById("med-" + consumerId);
        elem.srcObject.getTracks().forEach(function (track) {
            track.stop();
        });
        // elem.parentNode.removeChild(elem);
        elem.parentNode.remove();
        this.consumers.delete(consumerId);
    }

    async consume(data) {
        console.log("From consume", data);
        const { consumer, stream, kind } = data;
        this.consumers.set(consumer.id, consumer);
        let elem;
        let div;
        if (kind === 'video') {

            div = document.createElement('div');
            div.className = "col col-sm-4";
            div.id = consumer.id;

            elem = document.createElement('video');
            elem.srcObject = stream;
            elem.id = "med-" + consumer.id;
            elem.playsinline = false;
            elem.autoplay = true;
            elem.className = "vid";

            div.appendChild(elem);

            document.getElementById("remoteVideos").appendChild(div);
        } else {

            div = document.createElement('div');
            div.className = "col col-sm-4";
            div.id = consumer.id;

            elem = document.createElement('audio');
            elem.srcObject = stream;
            elem.id = "med-" + consumer.id;
            elem.playsinline = false;
            elem.autoplay = true;

            div.appendChild(elem);

            document.getElementById("remoteAudios").appendChild(div);
        }

        consumer.on('trackended', function () {
            this.removeConsumer(consumer.id);
        }.bind(this));
        consumer.on('transportclose', function () {
            this.removeConsumer(consumer.id);
        }.bind(this));
    }

    closeProducer(type) {
        if (!this.producerLabel.has(type)) {
            console.log('there is no producer for this type ' + type);
            return;
        }
        let producerId = this.producerLabel.get(type);
        this.room.send("producerClosed", {
            producerId
        });
        this.producers.get(producerId).close()
        this.producers.delete(producerId)
        this.producerLabel.delete(type)

        if (type !== this.mediaType.audio) {
            let elem = document.getElementById(producerId)
            elem.srcObject.getTracks().forEach(function (track) {
                track.stop()
            })
            elem.parentNode.removeChild(elem)
        }

        switch (type) {
            case this.mediaType.audio:
                // this.event(_EVENTS.stopAudio)
                break
            case this.mediaType.video:
                // this.event(_EVENTS.stopVideo)
                break
            case this.mediaType.screen:
                // this.event(_EVENTS.stopScreen)
                break;
            default:
                return
                break;
        }
    }

    async produce(type, deviceId = null) {
        let mediaConstraints = {};
        let audio = false;
        let screen = false;
        switch (type) {
            case this.mediaType.audio:
                mediaConstraints = {
                    audio: {
                        deviceId: deviceId
                    },
                    video: false
                }
                audio = true;
                break;
            case this.mediaType.video:
                mediaConstraints = {
                    audio: false,
                    video: {
                        width: {
                            min: 640,
                            ideal: 1920
                        },
                        height: {
                            min: 400,
                            ideal: 1080
                        },
                        deviceId: deviceId
                        /*aspectRatio: {
                            ideal: 1.7777777778
                        }*/
                    }
                }
                break
            case this.mediaType.screen:
                mediaConstraints = false;
                screen = true;
                break;
            default:
                return;
                break;
        }
        if (!this.device.canProduce('video') && !audio) {
            console.error('cannot produce video');
            return;
        }
        if (this.producerLabel.has(type)) {
            console.log('producer already exists for this type ' + type);
            return;
        }

        let stream;
        try {
            stream = screen
                ? await navigator.mediaDevices.getDisplayMedia()
                : await navigator.mediaDevices.getUserMedia(mediaConstraints);

            const track = audio
                ? stream.getAudioTracks()[0]
                : stream.getVideoTracks()[0];

            const params = {
                track
            };

            if (!audio && !screen) {
                params.encodings = [{
                    rid: 'r0',
                    maxBitrate: 100000,
                    //scaleResolutionDownBy: 10.0,
                    scalabilityMode: 'S1T3'
                },
                {
                    rid: 'r1',
                    maxBitrate: 300000,
                    scalabilityMode: 'S1T3'
                },
                {
                    rid: 'r2',
                    maxBitrate: 900000,
                    scalabilityMode: 'S1T3'
                }];

                params.codecOptions = {
                    videoGoogleStartBitrate: 1000
                };
            }

            var producer = await this.producerTransport.produce(params);
            this.producers.set(producer.id, producer);

            let elem;
            if (!audio) {
                elem = document.createElement('video');
                elem.srcObject = stream;
                elem.id = producer.id;
                elem.playsinline = false;
                elem.autoplay = true;
                elem.className = "vid col-md-12";
                document.getElementById("localMedia").appendChild(elem);
            }

            producer.on('trackended', () => {
                this.closeProducer(type);
            })

            producer.on('transportclose', function () {
                console.log('producer transport close');
                if (!audio) {
                    elem.srcObject.getTracks().forEach(function (track) {
                        track.stop();
                    })
                    elem.parentNode.removeChild(elem);
                }
                this.producers.delete(producer.id);

            }.bind(this));

            producer.on('close', function () {
                console.log('closing producer')
                if (!audio) {
                    elem.srcObject.getTracks().forEach(function (track) {
                        track.stop();
                    })
                    elem.parentNode.removeChild(elem);
                }
                this.producers.delete(producer.id);

            }.bind(this));

            this.producerLabel.set(type, producer.id);

            switch (type) {
                case this.mediaType.audio:
                    // this.event(_EVENTS.startAudio)
                    break
                case this.mediaType.video:
                    // console.log("aaaa");
                    // this.event(_EVENTS.startVideo)
                    break
                case this.mediaType.screen:
                    // this.event(_EVENTS.startScreen)
                    break;
                default:
                    return
                    break;
            }
        } catch (err) {
            console.error(err.message);
        }
    }

    /**
     * For game
     */

    /**For game */
    Message = function (arg) {
        this.text = arg.text, this.message_side = arg.message_side;
        this.avatar_color = arg.color
        // console.log('bolor', arg.color);
        this.draw = function (_this) {
            return function () {
                var $message;
                $message = $($('.message_template').clone().html());
                $message.addClass(_this.message_side).find('.text').html(_this.text);
                $message.find('.avatar').css('background-color', "#" + _this.avatar_color);
                $('.messages').append($message);
                return setTimeout(function () {
                    return $message.addClass('appeared');
                }, 0);
            };
        }(this);
        return this;
    };

    getMessageText = function () {
        var $message_input;
        $message_input = $('.message_input');
        return $message_input.val();
    };

    renderMatrix(state) {
        // if ($("#game-info-join").length) $("#game-info-join").remove();
        if (state.matrix
            && state.matrix.length > 0) {
            if ($("#game-info-join").length) $("#game-info-join").remove();
            var col = state.column;
            var row = state.row;
            var tableWidth = $("table").width();
            var cellHeight = (1 / col) * tableWidth;

            var matris = "<tr>";
            for (var i in state.matrix) {
                var bg = (state.opened_color[state.matrix[i]]) ? "background-color:#"
                    + state.opened_color[state.matrix[i]] + " !important" : "";
                matris += "<td onclick='room.sendMessage(\"fire\", {\"position\": "
                    + i + ", \"number\": " + state.matrix[i] + "})' style='"
                    + bg + "' class='align-middle text-center chose-box noselect' height='"
                    + cellHeight + "'>"
                    + "<b>" + state.matrix[i] + "</b>"
                    + "</td>";
                var index = parseInt(i) + 1;
                if ((index % col) == 0) {
                    if (index < state.matrix.length)
                        matris += "</tr><tr>";
                    else
                        matris += "</tr>";
                }
            }
            $("#matrix").html(matris);
            $("#next-number").html("<b>" + state.next + "</b>");
        }
    }

    renderSummary(state) {
        if (state.players) {
            var players = state.players;
            var summary = '';
            for (var id in players) {
                var you = '';
                var status = '';
                if (players[id].status == 'offline') status = '<span class="badge bg-secondary">Đã rời phòng</span>';
                if (this.room.sessionId == id) you = '<span class="badge bg-info text-dark">Bạn</span>'
                summary += '<li class="list-group-item d-flex justify-content-between align-items-right">'
                    + '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>'
                    + '<span class="badge"  style="background-color:#' + players[id].color + '">' + players[id].name + '</span>' + status + you
                    + '<span class="badge rounded-pill" style="background-color:#' + players[id].color + '">' + players[id].opened.length + '</span>'
            }
            summary = '<ul class="list-group">' + summary + '</ul>';
            $("#summary-id").html(summary);
        }
    }
    sendMessage(type, message) {
        if (!message) message = {};
        this.room.send(type, message);
    }

    rename() {
        var renameModal = new bootstrap.Modal(document.getElementById('renameModal'), {
            keyboard: false
        });
        $("#renameBtn").click(function () {
            var nickname = $("#nicknameInput").val();
            if (nickname && nickname != '') {
                this.sendMessage('rename', nickname);
                renameModal.hide();
            }
        }.bind(this));
        renameModal.show();
    }

    actionSendMessage() {
        var text = this.getMessageText();
        var $messages, message;
        if (text.trim() === '') {
            return;
        }
        $('.message_input').val('');
        $messages = $('.messages');
        var message_side = 'right';
        message = new this.Message({
            text: text,
            message_side: message_side,
            color: this.room.state.players.get(this.room.sessionId).color
        });
        this.sendMessage('chat', text);
        message.draw();
        return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
    };
    /**End for game */
}