const colyseus = require('colyseus');
const command = require("@colyseus/command");
const Room = require('../models/Room');

const MediasoupComponent = require('../components/mediasoup');
const { RoomState } = require('./schema/RoomState');

const { OnCreateCommand } = require('./commands/OnCreateCommand');
const { OnJoinCommand } = require('./commands/OnJoinCommand');
const { CreateWebRtcTransportCommand } = require('./commands/CreateWebRtcTransportCommnand');
const { GetProducersCommand } = require('./commands/GetProducersCommand');
const { ConsumeCommand } = require('./commands/ConsumeCommand');
const { ConnectTransportCommand } = require('./commands/ConnectTransportCommand');
const { ProduceCommand } = require('./commands/ProduceCommand');
const { ProducerCloseCommand } = require('./commands/ProducerCloseCommand');
const { OnLeaveCommand } = require('./commands/OnLeaveCommand');
const { OnDisposeCommand } = require('./commands/OnDisposeCommand');
const { StartCommand } = require('./commands/StartCommand');
const { FireCommand } = require('./commands/FireCommand');
const { RenameCommand } = require('./commands/RenameCommand');
const { ChatCommand } = require('./commands/ChatCommand');
const { TimingEventCommand } = require('./commands/TimingEventCommand');

exports.LiveRoom = class extends colyseus.Room {

    ffmpegCommand = null;

    async onCreate(options) {
        this.dispatcher = new command.Dispatcher(this);

        if (!options.id) return;
        var roomId = String(options.id);
        var roomData = await Room.findOne({ _id: roomId, status: Room.getStatus("CREATED") });
        if (!roomData) return;

        var roomStateData = roomData.toJSON();

        this.roomId = roomId;
        this.maxClients = roomData.max_client;

        var mds = new MediasoupComponent();
        roomStateData['worker'] = await mds.getWorker();

        this.setState(new RoomState(roomStateData));

        roomData.status = this.state.getStatus('WAITING');
        await roomData.save();

        // this.dispatcher.dispatch(new OnCreateCommand(), { room: this });

        this.dispatcher.dispatch(new TimingEventCommand(), {
            room: this,
            roomData
        });

        this.onMessage("createWebRtcTransport", async (client, message) => {
            this.dispatcher.dispatch(new CreateWebRtcTransportCommand(), {
                client,
                message,
                roomId
            });
        });

        this.onMessage("connectTransport", async (client, message) => {
            this.dispatcher.dispatch(new ConnectTransportCommand(), {
                client,
                message,
                roomId
            });
        });

        this.onMessage("getProducers", (client, message) => {
            this.dispatcher.dispatch(new GetProducersCommand(), {
                client,
                message,
                roomId
            });
        });

        this.onMessage("produce", async (client, message) => {
            var room = this;
            this.dispatcher.dispatch(new ProduceCommand(), {
                client,
                message,
                room
            });
        });

        this.onMessage("consume", (client, message) => {
            this.dispatcher.dispatch(new ConsumeCommand(), {
                client,
                message,
                roomId
            })
        });

        this.onMessage("producerClosed", (client, message) => {
            this.dispatcher.dispatch(new ProducerCloseCommand(), {
                client,
                message
            });
        });


        /**For game */
        this.onMessage("start", async (client, message) => {
            var room = this;
            this.dispatcher.dispatch(new StartCommand(), {
                client,
                message,
                room,
                roomData
            })
        });

        this.onMessage("fire", async (client, message) => {
            var room = this;
            this.dispatcher.dispatch(new FireCommand(), {
                client,
                message,
                room,
                roomData
            })
        });

        this.onMessage("rename", (client, message) => {
            this.dispatcher.dispatch(new RenameCommand(), {
                client,
                message
            })
        });

        this.onMessage("chat", (client, message) => {
            var room = this;
            this.dispatcher.dispatch(new ChatCommand(), {
                client,
                message,
                room
            })
        });
        /**End for game */
    }

    onJoin(client, options) {
        var roomId = this.roomId;
        this.dispatcher.dispatch(new OnJoinCommand(), {
            client,
            options,
            roomId
        });
    }

    onLeave(client, consented) {
        console.log(client.sessionId, "left!");
        this.dispatcher.dispatch(new OnLeaveCommand(), {
            client,
            consented
        });
    }

    async onDispose() {
        console.log("Room", this.roomId, "disposing...");
        var roomId = this.roomId;
        await this.dispatcher.dispatch(new OnDisposeCommand(), {
            roomId
        });
    }
}
