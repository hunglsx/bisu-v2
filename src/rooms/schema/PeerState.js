const schema = require('@colyseus/schema');
const MapSchema = schema.MapSchema;
const { types } = require('mediasoup');

class PeerState extends schema.Schema {
    constructor(options) {
        super();
        this.id = options.clientId;
        // this.name = options.clientName;
        this.transports = new MapSchema();
        this.consumers = new MapSchema();
        this.producers = new MapSchema();
    }
    addTransport(transport) {
        this.transports.set(transport.id, transport);
    }
    async connectTransport(transportId, dtlsParameters) {
        if (!this.transports.has(transportId)) return;
        await this.transports.get(transportId).connect({
            dtlsParameters: dtlsParameters
        });
    }

    async createProducer(producerTransportId, rtpParameters, kind) {
        //TODO handle null errors
        let producer = await this.transports.get(producerTransportId).produce({
            kind,
            rtpParameters
        });
        this.producers.set(producer.id, producer);

        producer.on('transportclose', function () {
            producer.close();
            this.producers.delete(producer.id);
        });

        return producer;
    }

    async createConsumer(consumerTransportId, producerId, rtpCapabilities) {
        let consumerTransport = this.transports.get(consumerTransportId);
        let consumer = null;
        try {
            consumer = await consumerTransport.consume({
                producerId: producerId,
                rtpCapabilities,
                paused: false //producer.kind === 'video',
            });
        } catch (error) {
            console.error('consume failed', error);
            return;
        }

        if (consumer.type === 'simulcast') {
            await consumer.setPreferredLayers({
                spatialLayer: 2,
                temporalLayer: 2
            });
        }

        this.consumers.set(consumer.id, consumer);

        consumer.on('transportclose', function () {
            this.consumers.delete(consumer.id);
        });

        return {
            consumer,
            params: {
                producerId: producerId,
                id: consumer.id,
                kind: consumer.kind,
                rtpParameters: consumer.rtpParameters,
                type: consumer.type,
                producerPaused: consumer.producerPaused
            }
        };
    }

    closeProducer(producerId) {
        try {
            this.producers.get(producerId).close();
        } catch (e) {
            console.warn(e);
        }
        this.producers.delete(producerId);
    }

    getProducer(producerId) {
        return this.producers.get(producerId);
    }

    close() {
        this.transports.forEach(transport => transport.close());
    }

    removeConsumer(consumer_id) {
        this.consumers.delete(consumer_id);
    }
}
schema.defineTypes(PeerState, {
    id: "string",
    name: "string",
    status: "string"
});
exports.PeerState = PeerState;