const schema = require('@colyseus/schema');
const MapSchema = schema.MapSchema;
const ArraySchema = schema.ArraySchema;
const { PeerState } = require('./PeerState');
const config = require('../../config');
const { types } = require('mediasoup');
const { RacingPlayerState } = require('./RacingPlayerState');

class RoomState extends schema.Schema {

    START_NUMBER = 0;
    STATUS_CREATED = 'created'
    STATUS_STARTED = 'started';
    STATUS_FINISHED = 'finished';
    STATUS_DISPOSED = 'disposed';
    STATUS_WAITING = 'waiting';

    routes = new MapSchema();
    plainTransports = new ArraySchema();
    plainProducers = new MapSchema();

    constructor(options) {
        super();
        this.peers = new MapSchema();
        this.name = options.name || '';
        this.status = options.status;
        this.maxClient = options.max_client;
        this.ffmpegCommand = '';
        if (options.worker) {
            options._id = String(options._id);
            const mediaCodecs = config.mediasoup.router.mediaCodecs;
            options.worker.createRouter({
                mediaCodecs
            }).then(function (router) {
                this.routes.set(options._id, router);
                // console.log("Router", this.routes.get(options._id));
            }.bind(this)).catch(error => {
                console.log("Contructor", error.message);
            });
        }

        /**For game */
        this.players = new MapSchema();
        this.row = options.row;
        this.column = options.column;
        this.next = this.START_NUMBER;
        this.opened = [];
        this.opened_color = {};
        var size = this.row * this.column;
        var halfpass = size / 2;
        if ((size % 2) != 0) {
            halfpass = Math.ceil(halfpass);
        } else {
            halfpass += 1;
        }
        this.halfpass = halfpass;
    }

    getStatus(status) {
        return this[`STATUS_${status}`];
    }

    setPlainTransport(transport) {
        this.plainTransports.push(transport);
    }

    getPlainTransport() {
        return this.plainTransports;
    }

    setPlainProducer(producer) {
        this.plainProducers.set(producer.id, producer);
    }

    getPlainProducer() {
        return this.plainProducers;
    }

    /**For game */
    createMatrix() {
        var numPosition = this.row * this.column;
        var matrixData = [];
        for (var i = 0; i < numPosition; i++) {
            matrixData.push(i);
        }
        matrixData = matrixData.sort(() => Math.random() - 0.5);
        this.matrix = matrixData;
    }

    reorderMatrix() {
        this.matrix = this.matrix.sort(() => Math.random() - 0.5);
    }
}

schema.defineTypes(RoomState, {
    peers: { map: PeerState },
    name: "string",
    status: "string",
    maxClient: "number",
    routes: { map: types.Router },
    ffmpegCommand: "string",

    /**For game */
    row: "uint8",
    column: "uint8",
    matrix: ["number"],
    opened: ["number"],
    left: ["number"],
    players: { map: RacingPlayerState },
    status: "string",
    curent: "number",
    next: "number",
    opened_color: { map: "string" },
    halfpass: "number"
});

exports.RoomState = RoomState;