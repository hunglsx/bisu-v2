const schema = require('@colyseus/schema');

class RacingPlayerState extends schema.Schema {
    constructor(options) {
        super();
        var letters = '0123456789ABCDEF';
        var colorCode = '';
        for (var i = 0; i < 6; i++) {
            colorCode += letters[Math.floor(Math.random() * 16)];
        }
        this.color = colorCode;
        this.name = this.color;
        this.opened = [];
        this.status = "online";
    }
}
schema.defineTypes(RacingPlayerState, {
    color: "string",
    opened: ["number"],
    name: "string",
    status: "string"
});
exports.RacingPlayerState = RacingPlayerState;