const command = require("@colyseus/command");

exports.TimingEventCommand = class TimingEventCommand extends command.Command {

    execute({ room, roomData } = this.payload) {
        if (roomData.level == 'Medium' || roomData.level == 'Hard') {
            var timeStep = (roomData.level == 'Medium') ? 10000 : 3000;
            room.clock.start();
            room.delayedInterval = room.clock.setInterval(function () {
                if (this.state.status == this.state.getStatus('STARTED')) {
                    this.state.reorderMatrix();
                }
            }.bind(this), timeStep);
        }
    }
}
