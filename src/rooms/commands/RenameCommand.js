const command = require("@colyseus/command");

exports.RenameCommand = class RenameCommand extends command.Command {

    execute({ client, message } = this.payload) {
        try {
            this.state.players.get(client.sessionId).name = String(message);
        } catch (error) {
            console.error(error.message);
        }
    }
}