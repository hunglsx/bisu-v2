const command = require("@colyseus/command");

exports.FireCommand = class FireCommand extends command.Command {

    validate({ message } = this.payload) {
        var number = parseInt(message.number);
        var position = parseInt(message.position);
        if (number == this.state.next
            && number == this.state.matrix[position]
            && this.state.status == this.state.getStatus('STARTED')) {
            return true;
        }
        return false;
    }

    async execute({ client, message, room, roomData } = this.payload) {
        try {
            var fireNumber = parseInt(message.number);
            this.state.current = fireNumber;
            this.state.players.get(client.sessionId).opened.push(fireNumber);
            this.state.opened.push(fireNumber);
            this.state.opened_color.set(String(fireNumber), this.state.players.get(client.sessionId).color);
            if ((this.state.next + 1) < (this.state.row * this.state.column)) this.state.next += 1;
            room.broadcast("fire", message, { except: client });
            if (this.isFinished(this.state)) {
                if (roomData.level == 'Medium' || roomData.level == 'Hard') room.delayedInterval.clear();
                this.state.status = this.state.getStatus('FINISHED');
                var summary = [];
                this.state.players.forEach((value, key) => {
                    summary.push({
                        'id': key,
                        'open': value.opened.length,
                        'color': value.color, name: value.name
                    });
                });
                summary.sort((a, b) => (a.open > b.open) ? -1 : 1);
                roomData.loadState(this.state);
                roomData.result = summary;
                await roomData.save();
                room.broadcast("finish", summary);
            }

        } catch (error) {
            console.log(error.message);
        }
    }

    isFinished() {
        if (this.state.opened.length == this.state.matrix.length) return true;
        var finished = false;
        this.state.players.forEach(function (value, key) {
            if (value.opened.length >= this.state.halfpass) {
                finished = true;
                return true;
            }
        }.bind(this));
        return finished;
    }
}