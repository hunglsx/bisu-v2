const command = require("@colyseus/command");

exports.ConsumeCommand = class ConsumeCommand extends command.Command {

    async execute({ client, message, roomId } = this.payload) {
        const {
            consumerTransportId,
            producerId,
            rtpCapabilities
        } = message;

        var params = await this.consume(client, consumerTransportId, producerId, rtpCapabilities, roomId);
        params['producerId'] = producerId;
        client.send("onConsume", params);
    }

    async consume(client, consumerTransportId, producerId, rtpCapabilities, roomId) {
        if (!this.state.routes.get(roomId).canConsume({
            producerId: producerId,
            rtpCapabilities,
        })) {
            console.error('can not consume');
            return;
        }
        var { consumer, params } = await this.state.peers.get(client.sessionId).createConsumer(consumerTransportId, producerId, rtpCapabilities);
        consumer.on('producerclose', function () {
            this.state.peers.get(client.sessionId).removeConsumer(consumer.id)
            client.send("consumerClosed", {
                consumerId: consumer.id
            });
        }.bind(this));
        return params;
    }
}