const command = require("@colyseus/command");

exports.ChatCommand = class ChatCommand extends command.Command {

    execute({ client, message, room } = this.payload) {
        try {
            var that = this;
            room.broadcast("receive_message", {
                message: message,
                color: that.state.players.get(client.sessionId).color
            }, { except: client });
        } catch (error) {
            console.error(error.message);
        }
    }
}