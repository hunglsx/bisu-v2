const command = require("@colyseus/command");
const Room = require('../../models/Room');

exports.OnDisposeCommand = class OnDisposeCommand extends command.Command {

    async execute({ roomId } = this.payload) {
        try {
            this.state.peers.forEach(function (peer) {
                peer.close();
                this.state.peers.delete(peer.id);
            }.bind(this));
            this.state.routes.forEach(router => {
                router.close();
            });
            await Room.updateOne({ _id: roomId }, { status: this.state.getStatus('DISPOSED') }).exec();
        } catch (error) {
            console.error(error.message);
        }
    }
}