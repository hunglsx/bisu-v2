const command = require("@colyseus/command");

exports.StartCommand = class StartCommand extends command.Command {

    async execute({ client, message, room, roomData } = this.payload) {
        try {
            this.state.createMatrix();
            this.state.status = this.state.getStatus('STARTED');
            roomData.status = this.state.status;
            await roomData.save();
            room.broadcast("start", message, { except: client });
        } catch (error) {
            console.log(error.message);
        }
    }
}