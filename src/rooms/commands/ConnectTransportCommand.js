const command = require("@colyseus/command");

exports.ConnectTransportCommand = class ConnectTransportCommand extends command.Command {

    async execute({ client, message, roomId } = this.payload) {
        try {
            await this.state.peers.get(client.sessionId)
                .connectTransport(message.transportId, message.dtlsParameters);

            client.send('onConnectTransport', 'success');
        } catch (error) {
            console.log(error.message);
        }
    }
}