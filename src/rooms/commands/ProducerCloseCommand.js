const command = require("@colyseus/command");

exports.ProducerCloseCommand = class ProducerCloseCommand extends command.Command {

    async execute({ client, message, room } = this.payload) {
        try {
            this.state.peers.get(client.sessionId).closeProducer(message.producerId);
        } catch (error) {
            console.error(error.message);
        }
    }
}