const command = require("@colyseus/command");

exports.GetProducersCommand = class GetProducersCommand extends command.Command {
    execute({ client, message, roomId } = this.payload) {
        try {
            // send all the current producer to newly joined member
            var producerList = this.getProducerListForPeer(client);
            console.log("getProducers", producerList);
            client.send("newProducers", producerList);
        } catch (error) {
            console.log(error.message);
        }
    }

    getProducerListForPeer(client) {
        let producerList = [];
        this.state.peers.forEach(peer => {
            if (peer.id != client.sessionId) {
                peer.producers.forEach(producer => {
                    producerList.push({
                        producerId: producer.id
                    });
                })
            }
        });
        var plainProducers = this.state.getPlainProducer();
        plainProducers.forEach(pProducer => {
            producerList.push({
                producerId: pProducer.id
            });
        });
        return producerList;
    }
}