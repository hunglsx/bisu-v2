const command = require("@colyseus/command");
const Room = require('../../models/Room');

exports.OnCreateCommand = class OnCreateCommand extends command.Command {

    async execute({ room } = this.payload) {
        const audioTransport = await this.state.routes.get(room.roomId).createPlainTransport({
            listenIp: '127.0.0.1',
            rtcpMux: false,
            comedia: true
        });
        // Read the transport local RTP port.
        const audioRtpPort = audioTransport.tuple.localPort;
        // Read the transport local RTCP port.
        const audioRtcpPort = audioTransport.rtcpTuple.localPort;

        console.log("Audio port:", "RTCP =>", audioRtcpPort, "RTP =>", audioRtpPort);
        this.state.setPlainTransport(audioTransport);

        const videoTransport = await this.state.routes.get(room.roomId).createPlainTransport({
            listenIp: '127.0.0.1',
            rtcpMux: false,
            comedia: true
        });
        // Read the transport local RTP port.
        const videoRtpPort = videoTransport.tuple.localPort;
        // Read the transport local RTCP port.
        const videoRtcpPort = videoTransport.rtcpTuple.localPort;

        console.log("Video port:", "RTCP =>", videoRtcpPort, "RTP =>", videoRtpPort);
        this.state.setPlainTransport(videoTransport);

        const audioProducer = await audioTransport.produce({
            kind: 'audio',
            rtpParameters: {
                codecs: [
                    {
                        mimeType: 'audio/opus',
                        clockRate: 48000,
                        payloadType: 101,
                        channels: 2,
                        rtcpFeedback: [],
                        parameters: { 'sprop-stereo': 1 }
                    }
                ],
                encodings: [{ ssrc: 11111111 }]
            }
        });

        const videoProducer = await videoTransport.produce({
            kind: 'video',
            rtpParameters: {
                codecs: [
                    {
                        mimeType: 'video/vp8',
                        clockRate: 90000,
                        payloadType: 102,
                        rtcpFeedback: [], // FFmpeg does not support NACK nor PLI/FIR.
                    }
                ],
                encodings: [{ ssrc: 22222222 }]
            }
        });

        this.state.setPlainProducer(audioProducer);
        this.state.setPlainProducer(videoProducer);

        this.state.ffmpegCommand = 'ffmpeg -re -v info -stream_loop -1 -i /home/hungls/Downloads/10000000.mp4 -map 0:a:0 -acodec libopus -ab 128k -ac 2 -ar 48000 -map 0:v:0 -pix_fmt yuv420p -c:v libvpx -b:v 1000k -deadline realtime -cpu-used 4 -f tee "[select=a:f=rtp:ssrc=11111111:payload_type=101]rtp://127.0.0.1:' + audioRtpPort + '?rtcpport=' + audioRtcpPort + '|[select=v:f=rtp:ssrc=22222222:payload_type=102]rtp://127.0.0.1:' + videoRtpPort + '?rtcpport=' + videoRtcpPort + '"';
        console.log("FFMPEG: ", 'ffmpeg -re -v info -stream_loop -1 -i /home/hungls/Downloads/10000000.mp4 -map 0:a:0 -acodec libopus -ab 128k -ac 2 -ar 48000 -map 0:v:0 -pix_fmt yuv420p -c:v libvpx -b:v 1000k -deadline realtime -cpu-used 4 -f tee "[select=a:f=rtp:ssrc=11111111:payload_type=101]rtp://127.0.0.1:' + audioRtpPort + '?rtcpport=' + audioRtcpPort + '|[select=v:f=rtp:ssrc=22222222:payload_type=102]rtp://127.0.0.1:' + videoRtpPort + '?rtcpport=' + videoRtcpPort + '"');
    }
}