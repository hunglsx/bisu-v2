const command = require("@colyseus/command");

exports.ProduceCommand = class ProduceCommand extends command.Command {

    async execute({ client, message, room } = this.payload) {
        try {
            var producerId = await this.produce(client, message.producerTransportId, message.rtpParameters, message.kind);
            room.broadcast("newProducers", [{
                producerId: producerId
            }], { except: client });
            client.send("onProduce", producerId);
        } catch (error) {
            console.error(error.message);
        }
    }

    async produce(client, producerTransportId, rtpParameters, kind) {
        return new Promise(async function (resolve, reject) {
            var producer = await this.state.peers.get(client.sessionId)
                .createProducer(producerTransportId, rtpParameters, kind);
            resolve(producer.id);
        }.bind(this));
    }
}