const command = require("@colyseus/command");

exports.OnLeaveCommand = class OnLeaveCommand extends command.Command {

    async execute({ client, message, room } = this.payload) {
        try {
            this.state.peers.get(client.sessionId).close();
            this.state.peers.delete(client.sessionId);
            //Gamer offline
            this.state.players.get(client.sessionId).status = "offline";
        } catch (error) {
            console.error(error.message);
        }
    }
}