const command = require("@colyseus/command");
const { PeerState } = require('../schema/PeerState');
const { RacingPlayerState } = require('../schema/RacingPlayerState');

exports.OnJoinCommand = class OnJoinCommand extends command.Command {

    execute({ client, options, roomId } = this.payload) {
        var peer = new PeerState({ clientId: client.sessionId });
        this.state.peers.set(client.sessionId, peer);
        client.send("onGetRouterRtpCapabilities", this.state.routes.get(roomId).rtpCapabilities);
        // client.send("ffmpeg", this.state.ffmpegCommand);

        var player = new RacingPlayerState(options);
        this.state.players.set(client.sessionId, player);
    }
}