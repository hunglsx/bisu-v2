const command = require("@colyseus/command");
const config = require('../../config');

exports.CreateWebRtcTransportCommand = class CreateWebRtcTransportCommand extends command.Command {
    async execute({ client, message, roomId } = this.payload) {
        try {
            console.log("createWebRtcTransport");
            const { params } = await this.createWebRtcTransport(client, roomId, message);
            if (message.type) {
                client.send(message.type, params);
            }
        } catch (error) {
            console.error("createWebRtcTransport", error.message);
        }
    }

    async createWebRtcTransport(client, roomId, message) {
        try {
            const {
                maxIncomingBitrate,
                initialAvailableOutgoingBitrate
            } = config.mediasoup.webRtcTransport;

            const transport = await this.state.routes.get(roomId).createWebRtcTransport({
                listenIps: config.mediasoup.webRtcTransport.listenIps,
                enableUdp: true,
                enableTcp: true,
                preferUdp: true,
                initialAvailableOutgoingBitrate,
            });
            if (maxIncomingBitrate) {
                await transport.setMaxIncomingBitrate(maxIncomingBitrate);
            }

            transport.on('dtlsstatechange', function (dtlsState) {
                if (dtlsState === 'closed') {
                    transport.close();
                }
            });

            transport.on('close', () => { });

            console.log('---ADDING TRANSPORT---', message.type, transport.id);

            this.state.peers.get(client.sessionId).addTransport(transport);

            return {
                params: {
                    id: transport.id,
                    iceParameters: transport.iceParameters,
                    iceCandidates: transport.iceCandidates,
                    dtlsParameters: transport.dtlsParameters
                },
            };
        } catch (error) {
            console.error("createWebRtcTransport", error.message);
            return;
        }
    }
}