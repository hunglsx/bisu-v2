const Arena = require("@colyseus/arena").default;
const { monitor } = require("@colyseus/monitor");

const config = require('./config');
const MongooseDriver = require("@colyseus/mongoose-driver").MongooseDriver;

var express = require('express');
var createError = require("http-errors");
var path = require('path');
var cookieParser = require('cookie-parser');
var cors = require('cors');
var RoomRouter = require('./routes/Room');
var MediasoupComponent = require('./components/mediasoup');

/**
 * Import your Room files
 */
const { LiveRoom } = require("./rooms/LiveRoom");

module.exports = Arena({
    getId: () => "STREAMHAY.DEV",

    initializeGameServer: async (gameServer) => {
        /**
         * Define your room handlers:
         */
        console.log("process.env.MEDIASOUP_ANNOUNCED_IP", process.env.MEDIASOUP_ANNOUNCED_IP);
        var msComponent = new MediasoupComponent(config);
        await msComponent.createWorkers();

        gameServer.drive = new MongooseDriver();
        gameServer.define('livestream', LiveRoom);

    },

    initializeExpress: (app) => {
        app.use(cors());
        app.use(express.static(path.join(__dirname, 'public')));
        app.use(express.json());
        app.use(express.urlencoded({ extended: false }));
        app.use(cookieParser());
        // view engine setup
        app.set('views', path.join(__dirname, 'views'));
        app.set('view engine', 'pug');
        /**
         * Bind your custom express routes here:
         */

        app.get("/", (req, res) => {
            return res.redirect("/room/create")
            res.send("It's time to kick ass and chew bubblegum!");
        });

        app.use("/room", RoomRouter);
        /**
         * Bind @colyseus/monitor
         * It is recommended to protect this route with a password.
         * Read more: https://docs.colyseus.io/tools/monitor/
         */
        app.use("/colyseus", monitor());

        // catch 404 and forward to error handler
        app.use(function (req, res, next) {
            next(createError(404));
        });
        // error handler
        app.use(function (err, req, res, next) {
            // set locals, only providing error in development
            res.locals.message = err.message;
            res.locals.error = req.app.get('env') === 'development' ? err : {};
            // render the error page
            res.status(err.status || 500);
            res.render('error');
        });
    },

    beforeListen: () => {
        /**
         * Before before gameServer.listen() is called.
         */
    }

});
