const mediasoup = require('mediasoup');

var workers = [];
var nextMediasoupWorkerIdx = 0;

function MediasoupComponent(options) {
    if (options) this.config = options;
}

MediasoupComponent.prototype.createWorkers = async function createWorkers() {
    try {
        // console.log("Before:", workers.length);
        if (workers && workers.length > 0) return true;
        if (!this.config) return true;

        let {
            numWorkers
        } = this.config.mediasoup

        for (let i = 0; i < numWorkers; i++) {
            let worker = await mediasoup.createWorker({
                logLevel: this.config.mediasoup.worker.logLevel,
                logTags: this.config.mediasoup.worker.logTags,
                rtcMinPort: this.config.mediasoup.worker.rtcMinPort,
                rtcMaxPort: this.config.mediasoup.worker.rtcMaxPort,
            });

            worker.on('died', () => {
                console.error('mediasoup worker died, exiting in 2 seconds... [pid:%d]', worker.pid);
                setTimeout(() => process.exit(1), 2000);
            });
            workers.push(worker);

            // log worker resource usage
            /*setInterval(async () => {
                const usage = await worker.getResourceUsage();
                console.info('mediasoup Worker resource usage [pid:%d]: %o', worker.pid, usage);
            }, 120000);*/
        }
        // console.log("After:", workers.length);
    } catch (error) {
        console.log(error.message);
    }
}

MediasoupComponent.prototype.getWorker = async function getWorker() {
    const worker = workers[nextMediasoupWorkerIdx];
    if (++nextMediasoupWorkerIdx === workers.length) {
        nextMediasoupWorkerIdx = 0;
    }
    return worker;
}

MediasoupComponent.prototype.getWorkerLength = async function getWorkerLength() {
    return workers.length;
}

module.exports = MediasoupComponent;