var express = require('express');
var router = express.Router();
var MediasoupComponent = require('../components/mediasoup');
var Room = require('../models/Room');

/* GET home page. */
router.get('/', async function (req, res, next) {
    try {
        return res.redirect('/room/create');
    } catch (error) {
        console.log(error);
    }
});

router.get('/create', function (req, res) {
    try {
        res.render('room/create', { id: req.params.id });
    } catch (error) {
        console.log(error);
    }
});

router.post('/create', async function (req, res) {
    try {
        var room = new Room(req.body);
        room.status = Room.getStatus("CREATED");
        if (await room.save()) {
            return res.redirect(`/room/${room._id}`);
        }
        return res.render('room/create', { id: req.params.id });
    } catch (error) {
        console.log(error);
    }
});

router.get('/join/:id', async function (req, res) {
    try {
        var room = await Room.findOne({
            _id: req.params.id,
            status: { $in: [Room.getStatus("STARTED"), Room.getStatus("CREATED"), Room.getStatus("WAITING")] }
        });
        if (room) {
            return res.render('room/join', { id: req.params.id });
        }
        return res.redirect('/room/create');
    } catch (error) {
        console.log(error);
    }
});

router.get('/:id', async function (req, res) {
    try {
        var room = await Room.findOne({
            _id: req.params.id,
            status: { $in: [Room.getStatus("STARTED"), Room.getStatus("CREATED")] }
        });
        if (room) {
            var joinUrl = 'https' + '://' + req.get('host') + '/room/join/' + req.params.id;
            var shareFacebook = "https://www.facebook.com/sharer/sharer.php?u=" + joinUrl;
            return res.render('room/host', { id: req.params.id, joinUrl: joinUrl, shareUrl: shareFacebook });
        }
        return res.redirect(`/room/create`);
    } catch (error) {
        console.log(error);
    }
});

module.exports = router;
